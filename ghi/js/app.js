function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
        <div class="card mb-3 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
          <small>${startDate} - ${endDate}</small>
        </div>
      </div>
    `;
  }

function formatDate(dateString) {
    const date = new Date(dateString);
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const year = date.getFullYear();

    return `${month}/${day}/${year}`;
}

function showAlert(message) {
    const alertContainer = document.querySelector('#alert-container');
    const html = `
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            ${message}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    `;
    alertContainer.innerHTML = html;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        const errorMessage = 'Error: ${response.status}';
        console.error(errorMessage);
        showAlert('An error occured while fetching conference data.')
      } else {
        const data = await response.json();
        let columnIndex = 1;

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = formatDate(details.conference.starts);
            const endDate = formatDate(details.conference.ends);
            const locationName = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, startDate, endDate, locationName);
            const column = document.querySelector(`#column${columnIndex}`);
            column.innerHTML += html;
            // console.log();

            columnIndex++;
            if (columnIndex > 3) {
              columnIndex = 1;
            }
          }
        }
      }
    } catch (e) {
      console.error(e);
      showAlert('An error occured while processing conference data.')
    }

});
