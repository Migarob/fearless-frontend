function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <a className="navbar-brand" href="#">Conference GO!</a>
        <div className="collapse navbar-collapse">
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link" href="#">Home</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">New location</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">New conference</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">New presentation</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
